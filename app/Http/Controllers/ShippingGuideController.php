<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Client;
use App\Models\Unit;
use App\Models\Role;
use App\utils\helpers;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class ShippingGuideController extends Controller
{
     //-------------------- Show All Shipping Guides  ------------------\\

     public function index(request $request)
     {
         $this->authorizeForUser($request->user('api'), 'view', ShippingGuide::class);
         $role = Auth::user()->roles()->first();
         $view_records = Role::findOrFail($role->id)->inRole('record_view');
 
         // How many items do you want to display.
         $perPage = $request->limit;
         $pageStart = \Request::get('page', 1);

         // Start displaying items from this number;
         $offSet = ($pageStart * $perPage) - $perPage;
         $order = $request->SortField;
         $dir = $request->SortType;
         $helpers = new helpers();

         // Filter fields With Params to retrieve
         $columns = array(0 => 'Ref', 1 => 'client_id', 2 => 'date');
         $param = array(0 => 'like', 1 => '=', 2 => '=');
         $data = array();
 
         // Check If User Has Permission View  All Records
         $Shippingguides = ShippingGuide::with('client')
             ->where('deleted_at', '=', null)
             ->where(function ($query) use ($view_records) {
                 if (!$view_records) {
                     return $query->where('user_id', '=', Auth::user()->id);
                 }
             });
 
         // Multiple Filter
         $Filtred = $helpers->filter($Shippingguides, $columns, $param, $request)

         // Search With Multiple Param
             ->where(function ($query) use ($request) {
                 return $query->when($request->filled('search'), function ($query) use ($request) {
                     return $query->where('Ref', 'LIKE', "%{$request->search}%")
                         ->orWhere(function ($query) use ($request) {
                             return $query->whereHas('client', function ($q) use ($request) {
                                 $q->where('name', 'LIKE', "%{$request->search}%");
                             });
                         });
                 });
             });
         $totalRows = $Filtred->count();
         if($perPage == "-1"){
             $perPage = $totalRows;
         }
         $Shippingguides = $Filtred->offset($offSet)
             ->limit($perPage)
             ->orderBy($order, $dir)
             ->get();
 
         foreach ($Shippingguides as $sg) {
             $item['id'] = $sg->id;
             $item['date'] = $sg->date;
             $item['Ref'] = $sg->Ref;
             $item['client_name'] = $sg['client']->name;
             $item['user_name'] = $sg['user'] ? $sg['user']->name : 'N/D';
             $item['total'] = $sg->total;
             $item['status'] = $sg->status;
             $data[] = $item;
         }
 
         $clients = client::where('deleted_at', '=', null)->get(['id', 'name']);
         $users = user::where('deleted_at', '=', null)->get(['id', 'name']);


          // Get warehouses assigned to user
          $user_auth = auth()->user();
          if($user_auth->is_all_warehouses){
             $warehouses = Warehouse::where('deleted_at', '=', null)->get(['id', 'name']);
          }
        
          else{
             $warehouses_id = UserWarehouse::where('user_id', $user_auth->id)->pluck('warehouse_id')->toArray();
             $warehouses = Warehouse::where('deleted_at', '=', null)->whereIn('id', $warehouses_id)->get(['id', 'name']);
          }
         return response()->json([
             'shippingguides' => $data,
             'totalRows' => $totalRows,
             'clients' => $warehouses,
             'users' => $warehouses,

         ]);
 
     }

     // Add shipping guide 
     public function store(Request $request)
     {
         $this->authorizeForUser($request->user('api'), 'create', ShippingGuide::class);
 
         request()->validate([
             'client_id' => 'required',
         ]);
 
         \DB::transaction(function () use ($request) {
             $order = new ShippingGuide;
 
             $order->date = $request->date;
             $order->Ref = $this->getNumberOrder();
             $order->client_id = $request->client_id;
             $order->comercial_desc = $request->comercial_desc;
             $order->vat = $request->vat;
             $order->total = $request->total;
             $order->status = $request->status;
             $order->note = $request->note;
             $order->user_id = Auth::user()->id;
 
             $order->save();
 
             $data = $request['details'];
             foreach ($data as $key => $value) {
                
                 //$unit = Unit::where('id', $value['purchase_unit_id'])->first();
                 $orderDetails[] = [
                     'ShippingGuide_id' => $order->id,
                     'quantity' => $value['quantity'],
                     'price' => $value['price'],
                     'discount' => $value['discount'],
                     'total_discount' => $value['total_discount'],                    
                     'vat' => $value['vat'],   
                     'total_vat' => $value['total_vat'],                    
                     'product_id' => $value['product_id'],
                     'total' => $value['total'],
                     'user_id' => $value['user_id']= Auth::user()->id,
                     'warehouse_id' => $value['warehouse_id'],
                 ];
 
                /* if ($order->status == "received") {
                     if ($value['product_variant_id'] !== null) {
                         $product_warehouse = product_warehouse::where('deleted_at', '=', null)
                             ->where('warehouse_id', $order->warehouse_id)
                             ->where('product_id', $value['product_id'])
                             ->where('product_variant_id', $value['product_variant_id'])
                             ->first();
 
                         if ($unit && $product_warehouse) {
                             if ($unit->operator == '/') {
                                 $product_warehouse->qte += $value['quantity'] / $unit->operator_value;
                             } else {
                                 $product_warehouse->qte += $value['quantity'] * $unit->operator_value;
                             }
                             $product_warehouse->save();
                         }
 
                     } else {
                         $product_warehouse = product_warehouse::where('deleted_at', '=', null)
                             ->where('warehouse_id', $order->warehouse_id)
                             ->where('product_id', $value['product_id'])
                             ->first();
 
                         if ($unit && $product_warehouse) {
                             if ($unit->operator == '/') {
                                 $product_warehouse->qte += $value['quantity'] / $unit->operator_value;
                             } else {
                                 $product_warehouse->qte += $value['quantity'] * $unit->operator_value;
                             }
                             $product_warehouse->save();
                         }
                     }
                 }*/
             }
             PurchaseDetail::insert($orderDetails);
         }, 10);
 
         return response()->json(['success' => true, 'message' => 'Shipping guide created !']);
     }
 
     //--------- Update shipping guide data  -------------\\
 
     public function update(Request $request, $id)
     {
         $this->authorizeForUser($request->user('api'), 'update', Purchase::class);
 
         request()->validate([
             'warehouse_id' => 'required',
             'supplier_id' => 'required',
         ]);
 
         \DB::transaction(function () use ($request, $id) {
             $role = Auth::user()->roles()->first();
             $view_records = Role::findOrFail($role->id)->inRole('record_view');
             $current_Purchase = Purchase::findOrFail($id);
 
             // Check If User Has Permission view All Records
             if (!$view_records) {
                 // Check If User->id === Purchase->id
                 $this->authorizeForUser($request->user('api'), 'check_record', $current_Purchase);
             }
 
             $old_purchase_details = PurchaseDetail::where('purchase_id', $id)->get();
             $new_purchase_details = $request['details'];
             $length = sizeof($new_purchase_details);
 
             // Get Ids for new Details
             $new_products_id = [];
             foreach ($new_purchase_details as $new_detail) {
                 $new_products_id[] = $new_detail['id'];
             }
 
             // Init Data with old Parametre
             $old_products_id = [];
             foreach ($old_purchase_details as $key => $value) {
                 $old_products_id[] = $value->id;
                
                 //check if detail has purchase_unit_id Or Null
                 if($value['purchase_unit_id'] !== null){
                     $unit = Unit::where('id', $value['purchase_unit_id'])->first();
                 }else{
                     $product_unit_purchase_id = Product::with('unitPurchase')
                     ->where('id', $value['product_id'])
                     ->first();
                     $unit = Unit::where('id', $product_unit_purchase_id['unitPurchase']->id)->first();
                 }
 
                 if($value['purchase_unit_id'] !== null){
                     if ($current_Purchase->statut == "received") {
 
                         if ($value['product_variant_id'] !== null) {
                             $product_warehouse = product_warehouse::where('deleted_at', '=', null)
                                 ->where('warehouse_id', $current_Purchase->warehouse_id)
                                 ->where('product_id', $value['product_id'])
                                 ->where('product_variant_id', $value['product_variant_id'])
                                 ->first();
 
                             if ($unit && $product_warehouse) {
                                 if ($unit->operator == '/') {
                                     $product_warehouse->qte -= $value['quantity'] / $unit->operator_value;
                                 } else {
                                     $product_warehouse->qte -= $value['quantity'] * $unit->operator_value;
                                 }
 
                                 $product_warehouse->save();
                             }
 
                         } else {
                             $product_warehouse = product_warehouse::where('deleted_at', '=', null)
                                 ->where('warehouse_id', $current_Purchase->warehouse_id)
                                 ->where('product_id', $value['product_id'])
                                 ->first();
 
                             if ($unit && $product_warehouse) {
                                 if ($unit->operator == '/') {
                                     $product_warehouse->qte -= $value['quantity'] / $unit->operator_value;
                                 } else {
                                     $product_warehouse->qte -= $value['quantity'] * $unit->operator_value;
                                 }
 
                                 $product_warehouse->save();
                             }
                         }
                     }
 
                     // Delete Detail
                     if (!in_array($old_products_id[$key], $new_products_id)) {
                         $PurchaseDetail = PurchaseDetail::findOrFail($value->id);
                         $PurchaseDetail->delete();
                     }
                 }
 
             }

}

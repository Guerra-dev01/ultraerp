<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingGuide extends Model
{

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'user_id','date', 'Ref', 'client_id', 'bill_to_pay', 'comercial_desc', 'vat', 
        'total', 'guide_type', 'note', 'date',

    ];

    protected $casts = [
        'user_id' => 'integer',
        'client_id' => 'integer',
    ];


    public function user()

    {
        return $this->belongsTo('App\Models\User');
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }
    //use HasFactory;
}

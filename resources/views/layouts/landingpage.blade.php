<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--###Titulo do site###-->
    <title>SiGERP</title>

    <!--##################Definicao de Estilos/CSS############################-->
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Estilo predefinido pelo Framework/Laravel-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!--#########################Estilos personalizados##################################-->
    <link href="{{asset('assets2/dist/css/styles.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    
 <!-- <link rel="icon" href="{{asset('assets2/uploads/thumbnail/clou_logo_thumb-100x72.png') }}">
  <link rel="apple-touch-icon" href="{{asset('assets2/img/apple-touch-icon.html') }}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets2/img/apple-touch-icon-72x72.html') }}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets2/apple-touch-icon-114x114.html') }}">-->
  <link rel="stylesheet" href="{{asset('assets2/admin/css/bootstrap.min.css') }}">
  
  
  <link rel="stylesheet" href="{{asset('assets2/front/css/cristal.minea19.css') }}" type="text/css">
  <link rel="stylesheet" href="{{asset('assets2/front/css/style.minea19.css') }}" type="text/css">

  <link rel="stylesheet" href="{{asset('assets2/admin/css/font-awesome.min.css') }}">
  <link href="{{asset('assets2/admin/css/toast.css') }}" rel="stylesheet" />
  <link href="{{asset('assets2/admin/css/sweet-alert.css') }}" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('assets2/front/css/simple-line-icons.css') }}">
  <link href="{{asset('assets2/front/css/select2.min.css') }}" rel="stylesheet" />
  <link href="{{asset('assets2/front/css/aos.css') }}" rel="stylesheet" />
</head>

 <body>
    <noscript>
        <strong>
          We're sorry but Stocky doesn't work properly without JavaScript
          enabled. Please enable it to continue.</strong
        >
      </noscript>
  
      <!-- built files will be auto injected -->
     
<!--.....##########Seccao de componentes do layout do landingpage###########.....-->
<div id="landingpage" style="background:lightblue">
    <!--##############Barra de navegacao (superior):Inicio##################-->
    <navbar-component></navbar-component>

    <main class="seccao-conteudo" role="main">
<!--#####################Conteudo do landingpage##################-->
        <div class="container-fluid" >          
            @yield('content')       
        </div>    

  <!--########################Footer (Rodape)############################-->
  <footer-component></footer-component>
    </main>      
    </div>

    
<!--############################SCRIPTS/JAVASCRIPT################################################.-->
      
<script src="/js/landingpage.min.js?v=4.0"></script>

<script src="{{asset('assets2/front/js/jquery-2.2.4.min.js')}}"></script>
        
<!-- Popper JS -->
<script src="{{asset('assets2/front/js/popper.min.js')}}"></script>

<!-- JavaScript compilado do bootstrap -->
<script src="{{asset('assets2/front/js/bootstrap.min.js')}}"></script>
    
<!-- gdpr compliance code -->
<script type="text/javascript" src="{{asset('assets2/front/js/jquery.cookieMessage.min.js')}}"></script>
<script type="text/javascript">
    var cookieMsg = $('.accept_cookies').val();
    var accept = $('.accept').val();
    $.cookieMessage({
        'mainMessage': cookieMsg+'<br>',
        'acceptButton': accept,
        'fontSize': '16px',
        'backgroundColor': '#222',
    });
</script>

<!-- gdpr compliance code -->
<script type="text/javascript" src="{{asset('assets2/front/js/main.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets2/front/js/custom5914.js?var=1.9&amp;time=1680595858')}}"></script>
<script src="{{asset('assets2/admin/js/toast.js')}}"></script>
<script src="{{asset('assets2/admin/js/sweet-alert.min.js')}}"></script>
<script src="{{asset('assets2/admin/js/validation.js')}}"></script>
<script src="{{asset('assets2/admin/js/select2.min.js')}}"></script>   
<script src="{{asset('assets2/front/js/aos.js')}}"></script>
<script>AOS.init();</script>
    
<script>
    $(document).ready(function() {
        $('.multiple_select').select2();
        $('.single_select').select2();
    });
</script>
    
<script type="text/javascript">
    
      </script>
    
<!--<script type="text/javascript" src="https://js.stripe.com/v2/"></script>-->
    
<!--<script type="text/javascript">
$(function() {
    
var $form         = $(".require-validation");
$('form.require-validation').bind('submit', function(e) {
var $form         = $(".require-validation"),
    inputSelector = ['input[type=email]', 'input[type=password]',
                     'input[type=text]', 'input[type=file]',
                     'textarea'].join(', '),
    $inputs       = $form.find('.required').find(inputSelector),
    $errorMessage = $form.find('div.error'),
    valid         = true;
    $errorMessage.addClass('hide');
    
    $('.has-error').removeClass('has-error');
$inputs.each(function(i, el) {
  var $input = $(el);
  if ($input.val() === '') {
    $input.parent().addClass('has-error');
    $errorMessage.removeClass('hide');
    e.preventDefault();
  } 
}); 
    
if (!$form.data('cc-on-file')) {
  $('.payment_loader').show();
  $('.payment_btn').prop("disabled", true);
    
  e.preventDefault();
  Stripe.setPublishableKey($form.data('stripe-publishable-key'));
  Stripe.createToken({
    number: $('.card-number').val(),
    cvc: $('.card-cvc').val(),
    exp_month: $('.card-expiry-month').val(),
    exp_year: $('.card-expiry-year').val()
  }, stripeResponseHandler);
}   
    
});   
    
function stripeResponseHandler(status, response) {
    if (response.error) {
        $('.payment_loader').hide();
        $('.payment_btn').prop("disabled", false);
    
        $('.error')
        .removeClass('hide')
        .find('.alert')
        .text(response.error.message);
        
        $.toast({
          heading: 'Error',
          text: response.error.message,
          position: 'top-right',
          loaderBg:'#fff',
          icon: 'error',
          hideAfter: 4500
        });
    
    } else {
        var token = response['id'];
        $form.find('input[type=text]').empty();
        $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
        $form.get(0).submit();
        
    }
}   
    
});     
</script>-->

      
<script type="text/javascript">
    var csrf_token = '7c227c58cc24e84cb7078a7a605e17ed';
    var token_name = 'csrf_test_name'
    </script>
    
    <script type="text/javascript">
     var _html = document.documentElement,
         isTouch = (('ontouchstart' in _html) || (navigator.msMaxTouchPoints > 0) || (navigator.maxTouchPoints));
     _html.className = _html.className.replace("no-js","js");
     _html.classList.add( isTouch ? "touch" : "no-touch");
    </script>
    <script type="text/javascript" src="{{asset('assets2/front/js/device.min.js')}}"></script>
    
</body>
</html>

@extends("layouts.landingpage")

@section("content")

<!--####Componentes do Landing Page####-->
<div class="py-10"></div>

<register-component></register-component>

<div class="py-8"></div>

@endsection
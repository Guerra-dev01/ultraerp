/* import specific icons */
import { 
    faUserSecret,faAdd, faFile, faDatabase, faTrash, faUser,faUsers,faRefresh, faHardDrive, faCartPlus, faCartArrowDown,
     faCartShopping, faCubes, faCog, faBoxes, faTh, faGear, faPrint,faBars, faFileCircleExclamation, faUpload, faSearch, faDesktop, faDashboard
 } from '@fortawesome/free-solid-svg-icons';

const icons ={
    faUserSecret,faAdd, faDatabase, faUser, faBoxes, faTrash, faUsers, faRefresh, faHardDrive, faCubes, faCartPlus, faCog, faTh, faGear,
    faBars, faFileCircleExclamation, faUpload, faPrint, faDesktop, faDashboard, faFile, faSearch, faCartArrowDown, faCartShopping

}

export default icons
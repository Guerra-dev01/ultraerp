import store from "./store";
import Vue from "vue";
import router from "./router";
import { ValidationObserver, ValidationProvider, extend, localize } from 'vee-validate';
import * as rules from "vee-validate/dist/rules";
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
Vue.use(BootstrapVue);
import Meta from "vue-meta";

Vue.use(router);

Vue.component(
  "lp-navbar",
  // The `import` function returns a Promise.
  () => import(/* webpackChunkName: "largeSidebar" */ "./containers/layouts/landingpage/Navbar.vue")
);

Vue.component(
  "customizer",
  // The `import` function returns a Promise.
  () => import(/* webpackChunkName: "customizer" */ "./components/common/customizer.vue")
);
Vue.component("vue-perfect-scrollbar", () =>
  import(/* webpackChunkName: "vue-perfect-scrollbar" */ "vue-perfect-scrollbar"));
/*Vue.component('navbar-component', require('./containers/layouts/landingpage/Navbar.vue').default);
Vue.component('jumbotron-component', require('/containers/layouts/landingpage/Jumbotron.vue').default);
Vue.component('footer-component', require('/containers/layouts/landingpage/Footer.vue').default);*/


Vue.use(Meta, {
  keyName: "metaInfo",
  attribute: "data-vue-meta",
  ssrAttribute: "data-vue-meta-server-rendered",
  tagIDKeyName: "vmid",
  refreshOnceOnNavigation: true
});

localize({
  en: {
    messages: {
      required: 'This field is required',
      required_if: 'This field is required',
      regex: 'This field must be a valid',
      mimes: `This field must have a valid file type.`,
      size: (_, { size }) => `This field size must be less than ${size}.`,
      min: 'This field must have no less than {length} characters',
      max: (_, { length }) => `This field must have no more than ${length} characters`
    }
  },
});
// Install VeeValidate rules and localization
Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});

// Register it globally
Vue.component("ValidationObserver", ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);

window.axios = require('axios');
window.axios.defaults.baseURL = '';

window.axios.defaults.withCredentials = true;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

axios.interceptors.response.use((response) => {

  return response;
}, (error) => {
  if (error.response && error.response.data) {
    if (error.response.status === 401) {
      window.location.href='/welcome';
    }

    if (error.response.status === 404) {
      router.push({ name: 'NotFound' });
    }
    if (error.response.status === 403) {
      router.push({ name: 'not_authorize' });
    }

    return Promise.reject(error.response.data);
  }
  return Promise.reject(error.message);
});

window.Fire = new Vue();

import { i18n } from "./plugins/i18n";

Vue.component('navbar-component', require('./containers/layouts/landingpage/Navbar.vue').default);
Vue.component('jumbotron-component', require('./containers/layouts/landingpage/Jumbotron.vue').default);

//Register User Step1
Vue.component('register-component', require('./views/app/landingpage/Register.vue').default);

//Register User Step2
Vue.component('registerstep2-component', require('./views/app/landingpage/RegisterStep2.vue').default);



Vue.config.productionTip = true;
Vue.config.silent = true;
Vue.config.devtools = false;

var landingpage = new Vue({
   
  el: '#landingpage',
  store,
  i18n,
  router:router,  
});


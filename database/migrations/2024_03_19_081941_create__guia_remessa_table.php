<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuiaRemessaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ShippingGuides', function (Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->integer('id', true);           
            $table->integer('client_id')->index('client_id_shippingguide'); 
            $table->integer('user_id')->nullable()->index('user_id_shippingguide');
            $table->string('Ref', 10); 
            $table->float('comercial_desc', 10, 0);
            $table->float('vat', 10, 0);
            $table->float('total', 10, 0);
            $table->string('note', 100);
            $table->date('date')->nullable();
            $table->string('status', 15);
            $table->integer('warehouse_id')->nullable()->index('warehouse_id_shippingguide');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ShippingGuides');
    }
}

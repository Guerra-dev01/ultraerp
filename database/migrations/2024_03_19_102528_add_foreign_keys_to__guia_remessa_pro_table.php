<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToGuiaRemessaProTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ShippingGuide_Details', function (Blueprint $table) {
            //
            $table->foreign('shippingGuide_id', 'shippingGuide_id_shippingguidepro')->references('id')->on('ShippingGuides')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('product_id', 'product_id_shippingguidepro')->references('id')->on('products')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('user_id', 'user_id_shippingguidepro')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ShippingGuide_Details', function (Blueprint $table) {
            //
            $table->dropForeign('shippingGuide_id_shippingguidepro');
            $table->dropForeign('product_id_shippingguidepro');
            $table->dropForeign('user_id_shippingguidepro');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToGuiaRemessaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ShippingGuides', function (Blueprint $table) {
            //
            $table->foreign('client_id', 'client_id_shippingguide')->references('id')->on('clients')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('user_id', 'user_id_shippingguide')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('warehouse_id', 'warehouse_id_shippingguide')->references('id')->on('warehouses')->onUpdate('RESTRICT')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ShippingGuides', function (Blueprint $table) {
            //
            $table->dropForeign('client_id_shippingguide');
            $table->dropForeign('user_id_shippingguide');
            $table->dropForeign('warehouse_id_shippingguide');
        });
    }
}

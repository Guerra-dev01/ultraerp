<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuiaRemessaProTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ShippingGuide_Details', function (Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->integer('id', true);    
            $table->integer('shippingGuide_id')->index('shippingGuide_id_shippingguidepro');       
            $table->integer('product_id')->index('product_id_shippingguidepro');
            $table->float('quantity'); 
            $table->float('price', 10, 0);
            $table->float('vat', 10, 0);
            $table->float('discount', 10, 0);
            $table->float('total', 10, 0);
            $table->float('total_descount', 10, 0);
            $table->float('total_vat', 10, 0);
            $table->integer('user_id')->nullable()->index('user_id_shippingguidepro'); 
            $table->timestamps();  
            $table->softDeletes();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ShippingGuide_Products');
    }
}

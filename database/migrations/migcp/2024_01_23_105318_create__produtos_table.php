<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Produtos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->integer('id', true);
			$table->string('CodProduto', 192); //code
            $table->integer('CodCategoria')->index('categoryp_id'); //category_id
            $table->integer('CodigoIVA')->nullable()->index('iva_id_products');
            $table->integer('CodUnidade')->nullable()->index('unitp_id_products'); //Units
            $table->integer('CodFornecedor')->index('providerp_id_products'); //provide_id
            $table->integer('CodUtil')->nullable()->index('userp_id_products'); //provide_id
            $table->string('CodigoBarra', 192)->nullable();
            $table->string('Type_barcode', 192)->nullable();
            $table->string('MotivoIsencao', 192)->nullable();
            $table->string('NomeProduto', 192);
            $table->string('Tipo', 20);
            $table->string('Descricao', 250)->nullable(); //note
            $table->float('PrecoVenda', 10, 0); //price
            $table->float('Desconto', 10, 0); //Discount
            $table->integer('Quantidade')->nullable()->default(0);
            $table->integer('QuantidadeSaida')->nullable()->default(0);
			$table->float('PrecoCusto', 10, 0);
            $table->integer('EstMax'); //stockmax
            $table->integer('EstMin'); //stockmax
			$table->integer('brand_id')->nullable()->index('brandprod_id_products');
			$table->integer('unit_sale_id')->nullable()->index('unitp_id_sales');
			$table->integer('unit_purchase_id')->nullable()->index('unitp_purchase_products');
			$table->float('TaxNet', 10, 0)->nullable()->default(0);
			$table->string('tax_method', 192)->nullable()->default('1');
			$table->text('image')->nullable();
			$table->float('stock_alert', 10, 0)->nullable()->default(0);
			$table->boolean('is_variant')->default(0);
			$table->boolean('is_active')->nullable()->default(1);
            $table->date('DataCadastro')->nullable();
            $table->string('Status')->nullable()->default('active'); //statusp
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Produtos');
    }
}

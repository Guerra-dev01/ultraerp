<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransferenciaInternaProTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TransferenciaInternaPro', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->integer('id', true);
        $table->integer('IdTrans')->index('transfinterna_id_transfinternapro');
        $table->integer('CodProduto')->index('product_id_transfinternapro');
        $table->string('Observacao', 100)->nullable();
        $table->date('Data')->nullable();
        $table->timestamps();
        $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TransferenciaInternaPro');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToAquisicaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Aquisicao', function (Blueprint $table) {
            //
            $table->foreign('CodFornecedor', 'provider_id_purchase')->references('id')->on('providers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('CodUtil', 'user_id_purchase')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Aquisicao', function (Blueprint $table) {
            //
            $table->dropForeign('provider_id_purchase');
            $table->dropForeign('user_id_purchase');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            //
            $table->string('description')->after('name');
            $table->string('type_product')->after('description');
          //  $table->integer('code_iva')->after('unit_id')->nullable()->index('iva_id_products');
             $table->integer('barcode')->after('Type_barcode')->nullable();
          //  $table->integer('code_user')->after('provider_id')->nullable()->index('user_id_products');
            $table->float('discount', 10, 0)->after('price')->default(0);
            $table->integer('quantity_entry')->after('discount')->default(0);
            $table->integer('quantity_exit')->after('quantity_entry')->default(0);
            $table->integer('stockmin')->after('cost');
            $table->integer('stockmax')->after('stockmin');
            $table->string('statusp')->after('is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}

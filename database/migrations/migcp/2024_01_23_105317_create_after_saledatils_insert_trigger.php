<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAfterSaledatilsInsertTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER after_saledatils_insert AFTER INSERT ON `sale_details` FOR EACH ROW
            BEGIN
            update sales
            set new.quantity = sale_details.quantity
            where id=sale_details.sale_id ;
            END
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP TRIGGER `after_saledatils_insert`");
    }
}

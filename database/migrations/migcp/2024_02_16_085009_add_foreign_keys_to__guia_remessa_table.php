<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToGuiaRemessaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('GuiaRemessa', function (Blueprint $table) {
            //
			$table->foreign('CodCliente', 'client_id_guiaremessa')->references('id')->on('clients')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('CodUtil', 'user_id_guiaremessa')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('GuiaRemessa', function (Blueprint $table) {
            //
            $table->dropForeign('client_id_guiaremessa');
            $table->dropForeign('user_id_guiaremessa');

        });
    }
}

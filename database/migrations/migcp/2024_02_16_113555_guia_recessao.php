<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GuiaRecessao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GuiaRecessao', function(Blueprint $table)
		{ //
        $table->engine = 'InnoDB';
			$table->integer('id', true);
            $table->integer('NumGuia');
			$table->integer('CodFornecedor')->index('provider_id_guiarecessao');
			$table->integer('CodUtil')->index('user_id_guiarecessao');
			$table->string('Ref', 10)->nullable();
            $table->string('ContaAPagar', 5)->nullable();
			$table->float('DescComercial')->nullable();
			$table->float('Iva')->nullable();
            $table->float('Total')->nullable();
            $table->string('Tipo', 10)->nullable();
            $table->float('Numerario')->nullable();
            $table->float('MPesa')->nullable();
			$table->string('Observacao', 100)->nullable();
            $table->date('Data')->nullable();
            $table->string('Status')->nullable()->default("active");
			$table->timestamps();
			$table->softDeletes();
    });
}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('GuiaRecessao');   

    }
}

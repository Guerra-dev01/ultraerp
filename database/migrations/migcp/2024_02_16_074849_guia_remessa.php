<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GuiaRemessa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GuiaRemessa', function(Blueprint $table)
		{
        // Table fields definition

           $table->engine = 'InnoDB';
			$table->integer('id', true);
            $table->integer('NumGuia');
			$table->integer('CodCliente')->index('client_id_guiaremessa');
			$table->integer('CodUtil')->index('user_id_guiaremessa');
			$table->string('Ref', 10)->nullable();
			$table->float('DescComercial')->nullable();
			$table->float('Iva')->nullable();
            $table->float('Total')->nullable();
			$table->string('Observacao', 100)->nullable();
            $table->date('Data')->nullable();
            $table->string('Status')->nullable()->default("active");
			$table->timestamps();
			$table->softDeletes();
    });
}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('GuiaRemessa');   
     }
}

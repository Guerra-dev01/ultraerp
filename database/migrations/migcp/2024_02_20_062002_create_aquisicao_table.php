<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAquisicaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Aquisicao', function (Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->integer('id', true);
			$table->integer('CodFornecedor')->index('client_id_purchase');
			$table->integer('CodUtil')->index('user_id_purchase');
			$table->string('Observacao', 100)->nullable();
            $table->date('DtEmissao')->nullable();
            $table->date('DtFinal')->nullable();
            $table->string('Status')->nullable()->default("active");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Aquisicao');
    }
}

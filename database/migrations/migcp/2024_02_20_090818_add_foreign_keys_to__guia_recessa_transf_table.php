<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToGuiaRecessaTransfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('GuiaRecessaTransf', function (Blueprint $table) {
            //
            $table->foreign('IdTrans', 'transfinterna_id_guiarecessatransf')->references('id')->on('transferenciainterna')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('CodUtil', 'user_id_guiarecessatransf')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('GuiaRecessaTransf', function (Blueprint $table) {
            //
            $table->dropForeign('transfinterna_id_guiarecessatransf');
            $table->dropForeign('user_id_guiarecessatransf');
        });
    }
}

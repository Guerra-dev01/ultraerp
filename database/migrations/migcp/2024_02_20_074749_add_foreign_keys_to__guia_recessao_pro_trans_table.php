<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToGuiaRecessaoProTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('GuiaRecessaoProTrans', function (Blueprint $table) {
            //
            $table->foreign('CodProduto', 'product_id_guiarecessaoprotrans')->references('id')->on('Produtos')->onUpdate('RESTRICT')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('GuiaRecessaoProTrans', function (Blueprint $table) {
            //
            $table->dropForeign('product_id_guiarecessaoprotrans');
        });
    }
}

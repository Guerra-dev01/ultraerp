<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Produtos', function (Blueprint $table) {
            //
            $table->foreign('brand_id', 'brandprod_id_products')->references('id')->on('brands')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('CodCategoria', 'categoryp_id')->references('id')->on('categories')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('CodUnidade', 'unitp_id_products')->references('id')->on('units')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('unit_sale_id', 'unitp_id_sales')->references('id')->on('units')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('unit_purchase_id', 'unitp_purchase_products')->references('id')->on('units')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('CodFornecedor', 'providerp_id_products')->references('id')->on('providers')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('CodigoIVA', 'iva_id_products')->references('id')->on('iva')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Produtos', function (Blueprint $table) {
            //
            $table->dropForeign('brandprod_id_products');
			$table->dropForeign('categoryp_id');
			$table->dropForeign('unitp_id_products');
			$table->dropForeign('unitp_id_sales');
			$table->dropForeign('unitp_purchase_products');
			$table->dropForeign('providerp_id_products');
            $table->dropForeign('iva_id_products');

        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTransferenciaInternaProTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TransferenciaInternaPro', function (Blueprint $table) {
            //
            $table->foreign('IdTrans', 'transfinterna_id_transfinternapro')->references('id')->on('transferenciainterna')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('CodProduto', 'product_id_transfinternapro')->references('id')->on('Produtos')->onUpdate('RESTRICT')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TransferenciaInternaPro', function (Blueprint $table) {
            //
            $table->dropForeign('transfinterna_id_transfinternapro');
            $table->dropForeign('product_id_transfinternapro');
        });
    }
}

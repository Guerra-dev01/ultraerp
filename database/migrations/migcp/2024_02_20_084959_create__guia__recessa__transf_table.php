<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuiaRecessaTransfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GuiaRecessaTransf', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id', true);
            $table->integer('CodUtil')->index('user_id_guiarecessatransf');
            $table->integer('IdTrans')->index('transfinterna_id_guiarecessatransf');
            $table->string('remetente',100)->nullable();
            $table->string('ContaAPagar', 10);
            $table->float('Total');
            $table->string('Observacao', 100)->nullable();
            $table->date('Data')->nullable();
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('GuiaRecessaTransf');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToTransferenciaInternaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('TransferenciaInterna', function (Blueprint $table) {
            //
            $table->foreign('CodUtil', 'user_id_transfinterna')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('TransferenciaInterna', function (Blueprint $table) {
            //
            $table->dropForeign('user_id_transfinterna');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToGuiaRecessaoProTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('GuiaRecessaoPro', function (Blueprint $table) {
            //
            $table->foreign('CodProduto', 'product_id_guiarecessaopro')->references('id')->on('Produtos')->onUpdate('RESTRICT')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('GuiaRecessaoPro', function (Blueprint $table) {
            //
            $table->dropForeign('product_id_guiarecessaopro');

        });
    }
}

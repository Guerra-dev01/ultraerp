<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TransferenciaInterna extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('TransferenciaInterna', function(Blueprint $table)
		{
        //
        $table->engine = 'InnoDB';
        $table->integer('id', true);
        $table->integer('CodUtil')->index('user_id_transfinterna');
        $table->string('Observacao', 100)->nullable();
        $table->date('Data')->nullable();
        $table->timestamps();
        $table->softDeletes();
    });
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop("TransferenciaInterna");

    }
}

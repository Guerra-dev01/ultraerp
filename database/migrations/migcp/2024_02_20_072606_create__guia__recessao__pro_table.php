<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuiaRecessaoProTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('GuiaRecessaoPro', function (Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->integer('id', true);
			$table->integer('CodProduto')->index('product_id_guiarecessaopro');
            $table->float('Preco');
			$table->integer('Quantidade');
			$table->float('TaxaIva')->nullable();
            $table->float('TotalIva')->nullable();
            $table->float('Total')->nullable();
            $table->float('TotalDesconto')->nullable();
            $table->float('TaxaDesconto')->nullable();
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('GuiaRecessaoPro');
    }
}

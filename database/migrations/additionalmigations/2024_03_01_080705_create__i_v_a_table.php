<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIVATable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('IVA', function (Blueprint $table) {
            
            $table->engine = 'InnoDB';
			$table->integer('id', true);
            $table->string('IVADescricao', 10)->nullable(); 
			$table->string('Texto', 20)->nullable(); 
			$table->string('Moeda', 5)->nullable(); 
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('IVA');
    }
}
